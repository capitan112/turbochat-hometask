#import "UsersTableViewController.h"

@interface UsersTableViewController ()

@end

@implementation UsersTableViewController

- (id)init {
    self = [super init];
    if (self) {
        chatUsers = [[NSMutableArray alloc] initWithObjects:@"caiman",@"Divine", @"evanskiev", nil];
        [chatUsers addObject:@"Fedya"];
        [chatUsers addObject:@"freedom"];
        [chatUsers addObject:@"Gara"];
        [chatUsers addObject:@"fuego"];
        [chatUsers addObject:@"gu3st"];
        [chatUsers addObject:@"Helga"];
        [chatUsers addObject:@"Damio"];
        [chatUsers addObject:@"Div"];
        self.navigationController.toolbarHidden = YES;
        NSMutableString *numberOfUsers = [NSMutableString stringWithFormat:@"%d",[chatUsers count]];
        if ([chatUsers count] > 1 ) {
            [numberOfUsers appendString:@" users"];
        } else {
            [numberOfUsers appendString:@" user"];
        }
        self.navigationItem.title = numberOfUsers;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [chatUsers count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellIdentifier];
    }
    cell.textLabel.text = [chatUsers objectAtIndex:indexPath.row];

    return cell;
}

@end
