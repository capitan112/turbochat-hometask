#import "ChatTableViewController.h"

#define backgroundTag 102
#define CONTENTHEIGHT 32

@interface ChatTableViewController ()

@end

@implementation ChatTableViewController

- (id)initWithStyle:(UITableViewStyle)style {
    self = [super initWithStyle:style];
    if (self) {
        chatObjectsArray = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController setToolbarHidden:NO];
    UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc]
                                                      initWithTarget:self
                                                              action:@selector(respondToLongPress:)];
    [self.view addGestureRecognizer:longPressGesture];

    self.title = NSLocalizedString(@"TurboChat", @"TurboChat");
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self fillChatArray];
    textField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 245, CONTENTHEIGHT)];
    textField.delegate = self;
    textField.borderStyle = UITextBorderStyleRoundedRect;
    textField.placeholder = @"Message";
    UIBarButtonItem *buttonSend = [[UIBarButtonItem alloc]initWithTitle:@"Send"
                                                                  style:UIBarButtonItemStyleBordered
                                                                 target:self
                                                                 action:@selector(sendMessagePressed:)];
    UIBarButtonItem *textFieldItem = [[UIBarButtonItem alloc] initWithCustomView:textField];
    NSArray *toolbarItems = [[NSArray alloc] initWithObjects: textFieldItem, buttonSend, nil];
    [self setToolbarItems:toolbarItems animated:NO];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                   style:UIBarButtonItemStyleBordered
                                                                  target:nil
                                                                  action:nil];
    UIBarButtonItem *usersButton = [[UIBarButtonItem alloc] initWithTitle:@"Users"
                                                                   style:UIBarButtonItemStyleBordered
                                                                  target:self
                                                                   action:@selector(userViewSelected:)];
    [usersButton setImage:[UIImage imageNamed:@"Users"]];
    self.navigationItem.rightBarButtonItem = usersButton;
    self.navigationItem.leftBarButtonItem = backButton;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showKeyboard)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(hideKeyboard)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void) fillChatArray {
    NSDictionary *messageDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"[18:57]", @"time", @"X-Treem", @"nickname", @"оллоло поссоны", @"message", nil];
    [chatObjectsArray addObject: messageDictionary];
    messageDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"[18:58]", @"time", @"X-Treem", @"nickname", @"только что видал, как в метро бомж убил собаку", @"message", nil];
    [chatObjectsArray addObject: messageDictionary];
    messageDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"[18:58]", @"time", @"X-Treem", @"nickname", @"запахом", @"message", nil];
    [chatObjectsArray addObject: messageDictionary];
    messageDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"[18:59]", @"time", @"X-Treem", @"nickname", @"пострадало еще 47 очевидцев", @"message", nil];
    [chatObjectsArray addObject: messageDictionary];
    messageDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"[19:00]", @"time", @"X-Treem", @"nickname", @"некоторые ослепли", @"message", nil];
    [chatObjectsArray addObject: messageDictionary];
    messageDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"[19:01]", @"time", @"X-Treem", @"nickname", @"будьте бдительны", @"message", nil];
    [chatObjectsArray addObject: messageDictionary];
}

#pragma mark Gesture Recognizer methods

- (void)respondToLongPress:(UILongPressGestureRecognizer *) gestureRecognizer {
    if ([gestureRecognizer state] == UIGestureRecognizerStateBegan) {
        CGPoint location = [gestureRecognizer locationInView:[gestureRecognizer view]];
        dropVieMenu = [[UIView alloc] initWithFrame:CGRectMake(location.x, location.y, 200, 120)];
        UIToolbar *dropViewTop = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, dropVieMenu.frame.size.width, CONTENTHEIGHT)];
        UIToolbar *dropViewCenter = [[UIToolbar alloc] initWithFrame:CGRectMake(0, CONTENTHEIGHT, dropVieMenu.frame.size.width, CONTENTHEIGHT)];
        UIToolbar *dropViewButtom = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 2*CONTENTHEIGHT, dropVieMenu.frame.size.width, CONTENTHEIGHT)];
        dropViewTop.barStyle = UIBarStyleDefault;
        dropViewCenter.barStyle = UIBarStyleDefault;
        dropViewButtom.barStyle = UIBarStyleDefault;
        UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                     target:nil
                                                                                     action:nil];
        UIBarButtonItem *profileButton = [[UIBarButtonItem alloc] initWithTitle:@"Profile"
                                                                        style:UIBarButtonItemStyleBordered
                                                                       target:self
                                                                       action:@selector(profileTapped:)];
        dropViewTop.items = [NSArray arrayWithObjects:flexibleSpace, profileButton, flexibleSpace, nil];
        UIBarButtonItem *logoutButton = [[UIBarButtonItem alloc] initWithTitle:@"Logout"
                                                                          style:UIBarButtonItemStyleBordered
                                                                         target:self
                                                                         action:@selector(logoutTapped:)];
        dropViewCenter.items = [NSArray arrayWithObjects:flexibleSpace, logoutButton, flexibleSpace, nil];
        UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel"
                                                                         style:UIBarButtonItemStyleBordered
                                                                        target:self
                                                                        action:@selector(cancelTapped:)];
        dropViewButtom.items = [NSArray arrayWithObjects:flexibleSpace, cancelButton,flexibleSpace, nil];
        [dropVieMenu addSubview:dropViewTop];
        [dropVieMenu addSubview:dropViewCenter];
        [dropVieMenu addSubview:dropViewButtom];
        [self.view addSubview:dropVieMenu];
    }
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [chatObjectsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UILabel *msgText = nil;
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        msgText = [[UILabel alloc] init];
        msgText.backgroundColor = [UIColor clearColor];
        msgText.numberOfLines = 0;
        msgText.lineBreakMode = NSLineBreakByWordWrapping;
        msgText.font = [UIFont systemFontOfSize:14];
        [cell.contentView addSubview:msgText];
    } else {
        msgText = (UILabel *)[cell.contentView viewWithTag:backgroundTag];
    }

    NSString *message = [self assebleMessageByPathIndex:indexPath.row];
    CGRect textSize = [self MessageSize:message];
    msgText.frame = CGRectMake(5, 0, textSize.size.width, textSize.size.height);
    msgText.text = message;
    
    NSRange boldedTextLocationFrom = [message rangeOfString:@"]"];
    NSRange boldedTextLocationTo = [message rangeOfString:@": "];
    NSRange boldedTextRange = NSMakeRange(boldedTextLocationFrom.location + 1,
                                          boldedTextLocationTo.location - boldedTextLocationFrom.location);
    NSMutableAttributedString *boldText = [[NSMutableAttributedString alloc] initWithString:message];
    UIFont *boldFont = [UIFont fontWithName:@"Helvetica-Bold" size:14.0f];
    [boldText addAttribute:NSFontAttributeName value:boldFont range:boldedTextRange];
    msgText.attributedText = boldText;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *message = [self assebleMessageByPathIndex:indexPath.row];
    CGRect textSize = [self MessageSize:message];

    return textSize.size.height;
}

#pragma mark Assembling Message

- (NSString *)assebleMessageByPathIndex:(NSInteger)pathIndex {
    NSString *date = [[chatObjectsArray objectAtIndex:pathIndex] objectForKey:@"time"];
    NSString *nickName = [[chatObjectsArray objectAtIndex:pathIndex] objectForKey:@"nickname"];
    NSString *message = [[chatObjectsArray objectAtIndex:pathIndex] objectForKey:@"message"];
    NSMutableString *assebleMessage = [NSMutableString stringWithString:date];
    [assebleMessage appendString:@" "];
    [assebleMessage appendString:nickName];
    [assebleMessage appendString:@": "];
    [assebleMessage appendString:message];
    
    return assebleMessage;
}

- (CGRect)MessageSize:message {
    CGSize maximumLabelSize = CGSizeMake(320, CGFLOAT_MAX);
    UIFont *font = [UIFont fontWithName:@"Helvetica" size:16.0f];
    NSDictionary *options = @{ NSFontAttributeName: font};
    CGRect textSize = [message boundingRectWithSize:maximumLabelSize
                                            options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                         attributes:options
                                            context:nil];
    return textSize;
}

#pragma mark TextField Delegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self addMessage];
    return YES;
}

- (IBAction)sendMessagePressed:(id)sender {
    [self addMessage];
}

- (void)addMessage {
    textField.text = @"";
    [textField resignFirstResponder];
}

- (IBAction)userViewSelected:(id)sender {
    usersView.hidesBottomBarWhenPushed = YES;
    [self addMessage];
    [self.navigationController pushViewController:usersView animated:YES];
}

#pragma mark Keyboard Notification

- (void)showKeyboard {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView setAnimationDuration:0.3];
    [self.navigationController.toolbar setFrame:CGRectMake(0,
                                                           [UIScreen mainScreen].bounds.size.height - 255,
                                                           [UIScreen mainScreen].bounds.size.width,
                                                           44)];
    [UIView commitAnimations];
}

- (void)hideKeyboard {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView setAnimationDuration: 0.3];
    [self.navigationController.toolbar setFrame:CGRectMake(0,
                                                           [UIScreen mainScreen].bounds.size.height - 44,
                                                           [UIScreen mainScreen].bounds.size.width,
                                                           44)];
    [UIView commitAnimations];
}

#pragma mark Button pressed Action;

- (IBAction)profileTapped:(id)sender {
    [dropVieMenu removeFromSuperview];
    [self.navigationController pushViewController:registerViewController animated:NO];
}

- (IBAction)logoutTapped:(id)sender {
    exit(0);
}

- (IBAction)cancelTapped:(id)sender {
    [dropVieMenu removeFromSuperview];
}

@end

