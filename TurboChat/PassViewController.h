#import <UIKit/UIKit.h>
#import "ChatTableViewController.h"
#import "RegisterViewController.h"

@interface PassViewController : UIViewController <UITextFieldDelegate> {
    UITextField             *passwordField;
    UITextField             *loginField;
    UIButton                *enterButton;
    UIButton                *registrationButton;
    UILabel                 *chatSignature;
@public
    ChatTableViewController *chatViewController;
    RegisterViewController  *registerViewController;
}

@end
