#import <UIKit/UIKit.h>
#import "UsersTableViewController.h"
#import "RegisterViewController.h"

@class RegisterViewController;

@interface ChatTableViewController : UITableViewController <UITextFieldDelegate> {
    NSMutableArray *chatObjectsArray;
    UITextField    *textField;
    UIView         *dropVieMenu;
@public
    UsersTableViewController *usersView;
    RegisterViewController  *registerViewController;
}

- (NSString *)assebleMessageByPathIndex:(NSInteger)pathIndex;

@end
