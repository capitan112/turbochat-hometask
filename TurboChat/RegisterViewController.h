#import <UIKit/UIKit.h>
#import "ChatTableViewController.h"

@class ChatTableViewController;

@interface RegisterViewController : UIViewController <UITextFieldDelegate> {
    UITextField             *loginField;
    UITextField             *passwordField;
    UITextField             *confirmPasswordField;
    UIButton                *registerButton;
    UIButton                *cancelButton;
@public
    ChatTableViewController *chatViewController;
}

@end
