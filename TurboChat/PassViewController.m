#import "PassViewController.h"

#define CONTENTWIDTH 280
#define CONTENTHEIGHT 32

@interface PassViewController ()

@end

@implementation PassViewController

- (id)init {
    self = [super init];
    if (self) {
        chatSignature = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CONTENTWIDTH, CONTENTHEIGHT)];
        chatSignature.text = @"TurboChat";
        chatSignature.center = CGPointMake(160.0, 100.0);
        chatSignature.textAlignment = NSTextAlignmentCenter;
        chatSignature.font = [UIFont fontWithName:@"Helvetica" size:36];
        
        loginField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, CONTENTWIDTH, CONTENTHEIGHT)];
        loginField.center = CGPointMake(160.0, 160.0);
        loginField.placeholder = @"NickName";
        loginField.clearButtonMode = UITextFieldViewModeWhileEditing;
        loginField.delegate = self;
        loginField.borderStyle = UITextBorderStyleRoundedRect;
        
        passwordField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, CONTENTWIDTH, CONTENTHEIGHT)];
        passwordField.center = CGPointMake(160.0, 200.0);
        passwordField.placeholder = @"Password";
        passwordField.delegate = self;
        passwordField.borderStyle = UITextBorderStyleRoundedRect;
        
        enterButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        enterButton.frame = CGRectMake(0, 0, 100, CONTENTHEIGHT);
        [enterButton setTitle:@"Login" forState:UIControlStateNormal];
        enterButton.titleLabel.font = [UIFont systemFontOfSize:20];
        [enterButton addTarget:self action:@selector(enterToChat:) forControlEvents:UIControlEventTouchUpInside];
        enterButton.center = CGPointMake(70.0, 240.0);
        
        registrationButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        registrationButton.frame = CGRectMake(0, 0, 120, CONTENTHEIGHT);
        [registrationButton setTitle:@"Register" forState:UIControlStateNormal];
        registrationButton.titleLabel.font = [UIFont systemFontOfSize:20];
        [registrationButton addTarget:self action:@selector(enterToRegistration:)forControlEvents:UIControlEventTouchUpInside];
        registrationButton.center = CGPointMake(240.0, 240.0);

        [self.view addSubview:chatSignature];
        [self.view addSubview:loginField];
        [self.view addSubview:passwordField];
        [self.view addSubview:enterButton];
        [self.view addSubview:registrationButton];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
    [self.navigationController setToolbarHidden:YES];
}

#pragma mark TextField Delegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    BOOL shouldReturn = YES;
    
    if (textField == loginField) {

    } else if (textField == passwordField) {
        [enterButton sendActionsForControlEvents:UIControlEventTouchUpInside];
    }
    
    return shouldReturn;
}

#pragma mark TextField Buttons methods

- (IBAction)enterToChat:(id)sender {
    [passwordField resignFirstResponder];
    [self.navigationController pushViewController:chatViewController animated:YES];
}

- (IBAction)enterToRegistration:(id)sender {
    [passwordField resignFirstResponder];
    [self.navigationController pushViewController:registerViewController animated:YES];
}

@end
