#import "AppDelegate.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    UsersTableViewController *usersView = [UsersTableViewController new];
    ChatTableViewController *chatView = [ChatTableViewController new];
    chatView->usersView = usersView;
    
    RegisterViewController *registerViewController = [RegisterViewController new];
    registerViewController->chatViewController = chatView;
    chatView->registerViewController = registerViewController;
    registerViewController.view.backgroundColor = [UIColor whiteColor];
    
    PassViewController *passwordView = [PassViewController new];
    passwordView->chatViewController = chatView;
    passwordView->registerViewController = registerViewController;
    
    UINavigationController *rootNavigationView = [[UINavigationController alloc] initWithRootViewController:passwordView];

    [rootNavigationView setNavigationBarHidden:YES];
    [rootNavigationView setToolbarHidden:YES];
    self.window.rootViewController = rootNavigationView;
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;
}

- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
    return UIInterfaceOrientationMaskPortrait;
}


@end
