#import "RegisterViewController.h"

#define CONTENTWIDTH 280
#define CONTENTHEIGHT 32

@interface RegisterViewController ()

@end

@implementation RegisterViewController

- (id)init {
    self = [super init];
    if (self) {
        loginField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, CONTENTWIDTH, CONTENTHEIGHT)];
        loginField.center = CGPointMake(160.0, 120.0);
        loginField.placeholder = @"NickName";
        loginField.clearButtonMode = UITextFieldViewModeWhileEditing;
        loginField.delegate = self;
        loginField.borderStyle = UITextBorderStyleRoundedRect;
        
        passwordField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, CONTENTWIDTH, CONTENTHEIGHT)];
        passwordField.center = CGPointMake(160.0, 160.0);
        passwordField.placeholder = @"Password";
        passwordField.delegate = self;
        passwordField.borderStyle = UITextBorderStyleRoundedRect;
        
        confirmPasswordField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, CONTENTWIDTH, CONTENTHEIGHT)];
        confirmPasswordField.center = CGPointMake(160.0, 200.0);
        confirmPasswordField.placeholder = @"Confirm password";
        confirmPasswordField.delegate = self;
        confirmPasswordField.borderStyle = UITextBorderStyleRoundedRect;
        
        registerButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        registerButton.frame = CGRectMake(0, 0, 100, CONTENTHEIGHT);
        [registerButton setTitle:@"Register" forState:UIControlStateNormal];
        registerButton.titleLabel.font = [UIFont systemFontOfSize:20];
        [registerButton addTarget:self action:@selector(registerToChat:) forControlEvents:UIControlEventTouchUpInside];
        registerButton.center = CGPointMake(70.0, 240.0);
        
        cancelButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        cancelButton.frame = CGRectMake(0, 0, 120, CONTENTHEIGHT);
        [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
        cancelButton.titleLabel.font = [UIFont systemFontOfSize:20];
        [cancelButton addTarget:self action:@selector(cancelRegistration:)forControlEvents:UIControlEventTouchUpInside];
        cancelButton.center = CGPointMake(240.0, 240.0);

        [self.view addSubview:loginField];
        [self.view addSubview:passwordField];
        [self.view addSubview:confirmPasswordField];
        [self.view addSubview:registerButton];
        [self.view addSubview:cancelButton];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
    [self.navigationController setToolbarHidden:YES];
    self.view.backgroundColor = [UIColor clearColor];
}

#pragma mark TextField Delegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    BOOL shouldReturn = YES;
    
    if (textField == loginField) {
        
    } else if (textField == passwordField) {

    } else if (textField == confirmPasswordField) {
        [registerButton sendActionsForControlEvents:UIControlEventTouchUpInside];
    }
    
    return shouldReturn;
}

#pragma mark TextField Buttons methods

- (IBAction)registerToChat:(id)sender {
    [passwordField resignFirstResponder];
    [self.navigationController popViewControllerAnimated:NO];
    [self.navigationController pushViewController:chatViewController animated:YES];
}

- (IBAction)cancelRegistration:(id)sender {
    [passwordField resignFirstResponder];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
