//
//  main.m
//  TurboChat
//
//  Created by Капитан on 14.04.14.
//  Copyright (c) 2014 Hometask. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
