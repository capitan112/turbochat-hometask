#import <UIKit/UIKit.h>
#import "ChatTableViewController.h"
#import "UsersTableViewController.h"
#import "PassViewController.h"
#import "RegisterViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
